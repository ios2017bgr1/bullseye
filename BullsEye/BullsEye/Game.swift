//
//  Game.swift
//  BullsEye
//
//  Created by Sebastian Guerrero on 11/13/17.
//  Copyright © 2017 SG. All rights reserved.
//

import Foundation

class Game {
  
  private var score:Int
  private var target:Int = Int(arc4random_uniform(99) + 1)
  private var round:Int {
    didSet {
      target = Int(arc4random_uniform(99) + 1)
    }
  }
  
  init(){
    score = 0
    round = 1
  }
  
  func getValues() -> (score:Int, round:Int, target:Int) {
    return (score, round, target)
  }
  
  func play(sliderValue:Int) -> Int {
    
    
    var difference = target - sliderValue
    
    if difference < 0 {
      difference *= -1
    }
    
    round += 1
    
    switch difference {
    case 0:
      score += 100
      return 100
    case 1...3:
      score += 75
      return 75
    case 4...10:
      score += 50
      return 50
    default:
      return 0
    }
  }
  
  func restart(){
    round = 1
    score = 0
  }
}
















