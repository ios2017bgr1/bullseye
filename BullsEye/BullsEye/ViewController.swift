//
//  ViewController.swift
//  BullsEye
//
//  Created by Sebastian Guerrero on 11/13/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  //MARK:- Outlets
  @IBOutlet weak var value: UILabel!
  @IBOutlet weak var score: UILabel!
  @IBOutlet weak var roundG: UILabel!
  @IBOutlet weak var slider: UISlider!
  
  let game = Game()
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    updateValues()
  }
  
  
  //MARK:- Actions
  
  @IBAction func restartButtonPressed(_ sender: Any) {
    game.restart()
    updateValues()
  }
  
  @IBAction func playButtonPressed(_ sender: Any) {
    let sliderValue = Int(round(slider.value))
    let roundScore = game.play(sliderValue: sliderValue)
    
    let alert = UIAlertController(title: "Hey!", message: "El valor fue \(sliderValue) y tu puntaje \(roundScore)", preferredStyle: .alert)
    
    let accept = UIAlertAction(title: "Aceptar", style: .default) { (action) in
      self.updateValues()
    }
    
    alert.addAction(accept)
    
    present(alert, animated: true, completion: nil)
  }
  
  func updateValues(){
    let (gScore, gRound, gTarget) = game.getValues()
    
    value.text = "\(gTarget)"
    score.text = "\(gScore)"
    roundG.text = "\(gRound)"
  }
  
  

  
}

